require 'test_helper'

class PsychedsControllerTest < ActionController::TestCase
  setup do
    @psyched = psycheds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:psycheds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create psyched" do
    assert_difference('Psyched.count') do
      post :create, psyched: {  }
    end

    assert_redirected_to psyched_path(assigns(:psyched))
  end

  test "should show psyched" do
    get :show, id: @psyched
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @psyched
    assert_response :success
  end

  test "should update psyched" do
    patch :update, id: @psyched, psyched: {  }
    assert_redirected_to psyched_path(assigns(:psyched))
  end

  test "should destroy psyched" do
    assert_difference('Psyched.count', -1) do
      delete :destroy, id: @psyched
    end

    assert_redirected_to psycheds_path
  end
end
