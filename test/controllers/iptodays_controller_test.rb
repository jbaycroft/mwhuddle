require 'test_helper'

class IptodaysControllerTest < ActionController::TestCase
  setup do
    @iptoday = iptodays(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:iptodays)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create iptoday" do
    assert_difference('Iptoday.count') do
      post :create, iptoday: {  }
    end

    assert_redirected_to iptoday_path(assigns(:iptoday))
  end

  test "should show iptoday" do
    get :show, id: @iptoday
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @iptoday
    assert_response :success
  end

  test "should update iptoday" do
    patch :update, id: @iptoday, iptoday: {  }
    assert_redirected_to iptoday_path(assigns(:iptoday))
  end

  test "should destroy iptoday" do
    assert_difference('Iptoday.count', -1) do
      delete :destroy, id: @iptoday
    end

    assert_redirected_to iptodays_path
  end
end
