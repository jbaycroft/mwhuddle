require 'test_helper'

class ActionitemsControllerTest < ActionController::TestCase
  setup do
    @actionitem = actionitems(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:actionitems)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create actionitem" do
    assert_difference('Actionitem.count') do
      post :create, actionitem: {  }
    end

    assert_redirected_to actionitem_path(assigns(:actionitem))
  end

  test "should show actionitem" do
    get :show, id: @actionitem
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @actionitem
    assert_response :success
  end

  test "should update actionitem" do
    patch :update, id: @actionitem, actionitem: {  }
    assert_redirected_to actionitem_path(assigns(:actionitem))
  end

  test "should destroy actionitem" do
    assert_difference('Actionitem.count', -1) do
      delete :destroy, id: @actionitem
    end

    assert_redirected_to actionitems_path
  end
end
