require 'test_helper'

class OrsControllerTest < ActionController::TestCase
  setup do
    @or = ors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create or" do
    assert_difference('Or.count') do
      post :create, or: {  }
    end

    assert_redirected_to or_path(assigns(:or))
  end

  test "should show or" do
    get :show, id: @or
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @or
    assert_response :success
  end

  test "should update or" do
    patch :update, id: @or, or: {  }
    assert_redirected_to or_path(assigns(:or))
  end

  test "should destroy or" do
    assert_difference('Or.count', -1) do
      delete :destroy, id: @or
    end

    assert_redirected_to ors_path
  end
end
