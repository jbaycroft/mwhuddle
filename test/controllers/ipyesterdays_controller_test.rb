require 'test_helper'

class IpyesterdaysControllerTest < ActionController::TestCase
  setup do
    @ipyesterday = ipyesterdays(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ipyesterdays)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ipyesterday" do
    assert_difference('Ipyesterday.count') do
      post :create, ipyesterday: {  }
    end

    assert_redirected_to ipyesterday_path(assigns(:ipyesterday))
  end

  test "should show ipyesterday" do
    get :show, id: @ipyesterday
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ipyesterday
    assert_response :success
  end

  test "should update ipyesterday" do
    patch :update, id: @ipyesterday, ipyesterday: {  }
    assert_redirected_to ipyesterday_path(assigns(:ipyesterday))
  end

  test "should destroy ipyesterday" do
    assert_difference('Ipyesterday.count', -1) do
      delete :destroy, id: @ipyesterday
    end

    assert_redirected_to ipyesterdays_path
  end
end
