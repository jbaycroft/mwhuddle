Rails.application.routes.draw do
  resources :actionitems do 
    member {post :markcomplete}
    member {post :markincomplete}
  end
  resources :iptodays
  resources :ipyesterdays
  resources :eds
  resources :psycheds
  resources :ors
  root 'home#index'
  resources :home, :only => [:index] do
  	collection {get :archive}
  end
  resources :units 
end
