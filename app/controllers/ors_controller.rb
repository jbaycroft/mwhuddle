class OrsController < ApplicationController
  before_action :set_or, only: [:show, :edit, :update, :destroy]

  # GET /ors
  # GET /ors.json
  def index
    @ors = Or.all
  end

  # GET /ors/1
  # GET /ors/1.json
  def show
  end

  # GET /ors/new
  def new
    @or = Or.new
  end

  # GET /ors/1/edit
  def edit
  end

  # POST /ors
  # POST /ors.json
  def create
    @or = Or.new(or_params)

    respond_to do |format|
      if @or.save
        format.html { redirect_to @or, notice: 'Or was successfully created.' }
        format.json { render :show, status: :created, location: @or }
      else
        format.html { render :new }
        format.json { render json: @or.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ors/1
  # PATCH/PUT /ors/1.json
  def update
    respond_to do |format|
      if @or.update(or_params)
        format.html { redirect_to @or, notice: 'Or was successfully updated.' }
        format.json { render :show, status: :ok, location: @or }
      else
        format.html { render :edit }
        format.json { render json: @or.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ors/1
  # DELETE /ors/1.json
  def destroy
    @or.destroy
    respond_to do |format|
      format.html { redirect_to ors_url, notice: 'Or was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_or
      @or = Or.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def or_params
      params.require(:or).permit(:cases)
    end
end
