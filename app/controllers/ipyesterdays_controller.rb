class IpyesterdaysController < ApplicationController
  before_action :set_ipyesterday, only: [:show, :edit, :update, :destroy]

  # GET /ipyesterdays
  # GET /ipyesterdays.json
  def index
    @ipyesterdays = Ipyesterday.all
  end

  # GET /ipyesterdays/1
  # GET /ipyesterdays/1.json
  def show
  end

  # GET /ipyesterdays/new
  def new
    @ipyesterday = Ipyesterday.new
  end

  # GET /ipyesterdays/1/edit
  def edit
  end

  # POST /ipyesterdays
  # POST /ipyesterdays.json
  def create
    @ipyesterday = Ipyesterday.new(ipyesterday_params)

    respond_to do |format|
      if @ipyesterday.save
        format.html { redirect_to @ipyesterday, notice: 'Ipyesterday was successfully created.' }
        format.json { render :show, status: :created, location: @ipyesterday }
      else
        format.html { render :new }
        format.json { render json: @ipyesterday.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ipyesterdays/1
  # PATCH/PUT /ipyesterdays/1.json
  def update
    respond_to do |format|
      if @ipyesterday.update(ipyesterday_params)
        format.html { redirect_to @ipyesterday, notice: 'Ipyesterday was successfully updated.' }
        format.json { render :show, status: :ok, location: @ipyesterday }
      else
        format.html { render :edit }
        format.json { render json: @ipyesterday.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ipyesterdays/1
  # DELETE /ipyesterdays/1.json
  def destroy
    @ipyesterday.destroy
    respond_to do |format|
      format.html { redirect_to ipyesterdays_url, notice: 'Ipyesterday was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ipyesterday
      @ipyesterday = Ipyesterday.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ipyesterday_params
      params.require(:ipyesterday).permit(:plandcprenoon, :plandcafternoon, :actualdcprenoon, :actualdcafternoon, :admithib, :longestwaited, :longestwaitpacu)
    end
end
