class HomeController < ApplicationController
	before_action :define_days, only: [:index]
	before_action :generate_todays, only: [:index]
	before_action :def_categories, only: [:index, :archive]
	before_action :check_archive, only: [:index, :archive]
	#Today Index
	def index
		# Define Instance Variables for table generation
		@units = Unit.all
		#Define Today
		@today = Date.today
	end
	def archive
		# Define Instance Variables for table generation
		@units = Unit.all
		@day = params[:date].to_datetime.change(:offset => "-0500")
		@ed = Ed.where({ created_at: @day.noon..(@day.noon + 1.day) }).first
		@psyched = Psyched.where({ created_at: @day.noon..(@day.noon + 1.day) }).first
		@or = Or.where({ created_at: @day.noon..(@day.noon + 1.day) }).first
	end

	private 

		def generate_todays
		#Generate Non-Unit Specific Today Records for ED, Psyched, and OR
			if Ed.where({ created_at: @start..@end }).empty?
				@ed = Ed.create(:level => 0, :census => 0)
			else
				@ed = Ed.where({ created_at: @start..@end }).first
			end
			if Psyched.where({ created_at: @start ..@end }).empty?
				@psyched = Psyched.create(:lmh => 0, :fuh => 0, :stv => 0)
			else
				@psyched = Psyched.where({ created_at: @start..@end }).first
			end
			if Or.where({ created_at: @start..@end }).empty?
				@or = Or.create(:cases => 0)
			else
				@or = Or.where({ created_at: @start..@end }).first
			end
		#Generate Unit specific Today Records for IPToday and IPYesterday
			@units = Unit.all
			@units.each do |unit|
				if unit.iptodays.where({ created_at: @start..@end }).empty?
					unit.iptodays.create()
				end
				if unit.ipyesterdays.where({ created_at: @start..@end }).empty?
					unit.ipyesterdays.create()
				end
			end
		end
		def def_categories
			# Define Instance Variables for each of the 4 categories of action items
			@cat1s = Actionitem.where(:category => "cat1").where(:complete => false)
			@cat2s = Actionitem.where(:category => "cat2").where(:complete => false)
			@cat3s = Actionitem.where(:category => "cat3").where(:complete => false)
			@cat4s = Actionitem.where(:category => "cat4").where(:complete => false)
		end
		def check_archive
			count = 0
			(1..14).each do |i|
   				unless Ed.where({ created_at: i.day.ago.noon..(i.day.ago.noon + 1.day) }).empty?
   					count += 1
   				end
			end
			@archivecount =  count
		end

		def define_days
			# Define Today.noon
			today_noon = Time.zone.now.noon

			if today_noon.future?
				@start = (Time.zone.now.noon - 1.day)
				@end = Time.zone.now.noon
			elsif today_noon.past?
				@start = Time.zone.now.noon
				@end = (Time.zone.now.noon + 1.day)
			end
		end
end
