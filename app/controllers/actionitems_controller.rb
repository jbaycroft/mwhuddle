class ActionitemsController < ApplicationController
  before_action :set_actionitem, only: [:show, :edit, :update, :destroy, :markcomplete, :markincomplete]

  # GET /actionitems
  # GET /actionitems.json
  def index
    @actionitems = Actionitem.where(:complete => true)
  end

  # GET /actionitems/1
  # GET /actionitems/1.json
  def show
  end

  # GET /actionitems/new
  def new
    @actionitem = Actionitem.new(:complete => false)
  end

  # GET /actionitems/1/edit
  def edit
  end

  # POST /actionitems
  # POST /actionitems.json
  def create
    @actionitem = Actionitem.new(actionitem_params)

    respond_to do |format|
      if @actionitem.save
        format.html { redirect_to root_path, notice: 'Action Item was successfully created.' }
        format.json { render :show, status: :created, location: @actionitem }
      else
        format.html { render :new }
        format.json { render json: @actionitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /actionitems/1
  # PATCH/PUT /actionitems/1.json
  def update
    respond_to do |format|
      if @actionitem.update(actionitem_params)
        format.html { redirect_to root_path, notice: 'Action Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @actionitem }
      else
        format.html { render :edit }
        format.json { render json: @actionitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /actionitems/1
  # DELETE /actionitems/1.json
  def destroy
    @actionitem.destroy
    respond_to do |format|
      format.js
    end
  end

  # MARK AS COMPLETE
  def markcomplete
    respond_to do |format|
      if @actionitem.update(:complete => true)
        format.js
      else
        format.html { render :edit }
        format.json { render json: @actionitem.errors, status: :unprocessable_entity }
      end
    end
  end
  # MARK AS INCOMPLETE
  def markincomplete
    respond_to do |format|
      if @actionitem.update(:complete => false)
        format.html { redirect_to actionitems_path, notice: 'Action Item Marked as Complete' }
        format.json { render :show, status: :ok, location: @actionitem }
      else
        format.html { render :edit }
        format.json { render json: @actionitem.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_actionitem
      @actionitem = Actionitem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def actionitem_params
      params.require(:actionitem).permit(:description, :assignedto, :category, :duedate, :complete)
    end
end
