class PsychedsController < ApplicationController
  before_action :set_psyched, only: [:show, :edit, :update, :destroy]

  # GET /psycheds
  # GET /psycheds.json
  def index
    @psycheds = Psyched.all
  end

  # GET /psycheds/1
  # GET /psycheds/1.json
  def show
  end

  # GET /psycheds/new
  def new
    @psyched = Psyched.new
  end

  # GET /psycheds/1/edit
  def edit
  end

  # POST /psycheds
  # POST /psycheds.json
  def create
    @psyched = Psyched.new(psyched_params)

    respond_to do |format|
      if @psyched.save
        format.html { redirect_to @psyched, notice: 'Psyched was successfully created.' }
        format.json { render :show, status: :created, location: @psyched }
      else
        format.html { render :new }
        format.json { render json: @psyched.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /psycheds/1
  # PATCH/PUT /psycheds/1.json
  def update
    respond_to do |format|
      if @psyched.update(psyched_params)
        format.html { redirect_to @psyched, notice: 'Psyched was successfully updated.' }
        format.json { render :show, status: :ok, location: @psyched }
      else
        format.html { render :edit }
        format.json { render json: @psyched.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /psycheds/1
  # DELETE /psycheds/1.json
  def destroy
    @psyched.destroy
    respond_to do |format|
      format.html { redirect_to psycheds_url, notice: 'Psyched was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_psyched
      @psyched = Psyched.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def psyched_params
      params.require(:psyched).permit(:lmh, :fuh, :stv)
    end
end
