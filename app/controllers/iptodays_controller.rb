class IptodaysController < ApplicationController
  before_action :set_iptoday, only: [:show, :edit, :update, :destroy]

  # GET /iptodays
  # GET /iptodays.json
  def index
    @iptodays = Iptoday.all
  end

  # GET /iptodays/1
  # GET /iptodays/1.json
  def show
  end

  # GET /iptodays/new
  def new
    @iptoday = Iptoday.new
  end

  # GET /iptodays/1/edit
  def edit
  end

  # POST /iptodays
  # POST /iptodays.json
  def create
    @iptoday = Iptoday.new(iptoday_params)

    respond_to do |format|
      if @iptoday.save
        format.html { redirect_to @iptoday, notice: 'Iptoday was successfully created.' }
        format.json { render :show, status: :created, location: @iptoday }
      else
        format.html { render :new }
        format.json { render json: @iptoday.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /iptodays/1
  # PATCH/PUT /iptodays/1.json
  def update
    respond_to do |format|
      if @iptoday.update(iptoday_params)
        format.html { redirect_to @iptoday, notice: 'Iptoday was successfully updated.' }
        format.json { render :show, status: :ok, location: @iptoday }
      else
        format.html { render :edit }
        format.json { render json: @iptoday.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /iptodays/1
  # DELETE /iptodays/1.json
  def destroy
    @iptoday.destroy
    respond_to do |format|
      format.html { redirect_to iptodays_url, notice: 'Iptoday was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_iptoday
      @iptoday = Iptoday.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def iptoday_params
      params.require(:iptoday).permit(:staffedcap, :actualcensus, :boarders, :surgadmit, :plannedadmit, :plandcprenoon, :plandcafternoon, :adjustedcensus, :precautionpat, :singlegroup, :psych)
    end
end
