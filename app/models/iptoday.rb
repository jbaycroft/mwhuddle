class Iptoday < ActiveRecord::Base
	belongs_to :unit
	before_update :calculate_census

	def todaysrecords
		self.each do |r|
			return r.staffedcap
		end

	end

	private
	def calculate_census
		self.adjustedcensus = self.actualcensus + self.boarders + self.surgadmit + self.plannedadmit + self.plandcprenoon + self.plandcafternoon
	end
end
