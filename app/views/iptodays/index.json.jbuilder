json.array!(@iptodays) do |iptoday|
  json.extract! iptoday, :id
  json.url iptoday_url(iptoday, format: :json)
end
