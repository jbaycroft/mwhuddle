json.array!(@psycheds) do |psyched|
  json.extract! psyched, :id
  json.url psyched_url(psyched, format: :json)
end
