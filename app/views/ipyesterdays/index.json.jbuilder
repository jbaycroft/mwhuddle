json.array!(@ipyesterdays) do |ipyesterday|
  json.extract! ipyesterday, :id
  json.url ipyesterday_url(ipyesterday, format: :json)
end
