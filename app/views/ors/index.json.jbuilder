json.array!(@ors) do |or|
  json.extract! or, :id
  json.url or_url(or, format: :json)
end
