json.array!(@actionitems) do |actionitem|
  json.extract! actionitem, :id
  json.url actionitem_url(actionitem, format: :json)
end
