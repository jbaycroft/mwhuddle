
$(document).ready(function() {
  /* Activating Best In Place */
  jQuery(".best_in_place").best_in_place();
  /* Tab action in best in place */
    $(function() {
      $('span.best_in_place[data-html-attrs]').each(function() {
        var attrs, el;
        el = $(this);
        attrs = el.data('html-attrs');
        if (attrs && attrs['tabindex']) {
          el.attr('tabindex', attrs['tabindex']);
        }
      }).focus(function() {
        var el;
        el = $(this);
        el.click();
      });
    });
  var cdu = parseInt($('td.cdu span').html(),10);
  /* Calculating Adjusted Census and then adding styles based on values.  */
  $('tr.iptodays').each(function () {
    /* Define all necessary DOM variables in a hash to simplify on.click script */
      var $$ = {
              'sc': $('span[id*=staffedcap]', this),
              'ac': $('span[id*=actualcensus]', this),
              'b': $('span[id*=boarders]', this),
              'sa': $('span[id*=surgadmit]', this),
              'pa': $('span[id*=plannedadmit]', this),
              'post': $('span[id*=plandcafternoon]', this),
              'adc': $('span[id*=adjustedCensus]', this)
      };
      $('td').on('click', function () {
          var ac = parseInt($$.ac.html(),10),
              sc = parseInt($$.sc.html(),10),
              b = parseInt($$.b.html(),10),
              sa = parseInt($$.sa.html(),10),
              pa = parseInt($$.pa.html(),10),
              post = parseInt($$.post.html(),10),
              // Adjusted Census is equal to = 
              // Actual Census + Boarders + Surg Admit 
              // + Planned Admit - Discharge (D/C)
              adc = ac + b + sa + pa - post;
              if (adc > sc) {
              	$$.adc.parent('td').addClass("outofspec");
              } else if (adc <= cdu) {
              	$$.adc.parent('td').removeClass("outofspec");
              };
              $$.adc.html(adc);
      }).trigger('click');
  });
$('tr.ipyesterdays').each(function () {
  var $$ = {
    'pd' : $('span[id*=plandcafternoon]', this),
    'ad' : $('span[id*=actualdcafternoon]', this)
  };
  $('td').on('click', function () {
    var pd = parseInt($$.pd.html(),10),
        ad = parseInt($$.ad.html(),10);
        // If Actual D/C is <= Planned D/C then out of spec
        if (ad < pd) {
          $$.ad.parent('td').addClass("outofspec");
        } else if (ad >= pd) {
          $$.ad.parent('td').removeClass("outofspec");
        };
  }).trigger('click');
});


  $('div.ed').on('click', function () {
    leveltd = $('span[id*=_level]');
    level = parseInt(leveltd.html(),10);
    if (level <= 1) {
      leveltd.parent('td').removeClass("outofspec");
      leveltd.parent('td').removeClass("yellowLight");
      leveltd.parent('td').addClass("greenLight");
    } else if (level == 2) {
      leveltd.parent('td').removeClass("greenLight");
      leveltd.parent('td').removeClass("outofspec");
      leveltd.parent('td').addClass("yellowLight");
    } else if (level >= 3) {
      leveltd.parent('td').removeClass("yellowLight");
      leveltd.parent('td').removeClass("greenLight");
      leveltd.parent('td').addClass("outofspec");
    };

  }).trigger('click');




});