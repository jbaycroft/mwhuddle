# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Unit.create(name: '5N')
Unit.create(name: '5S')
Unit.create(name: 'ICU')
Unit.create(name: 'CDU')
Unit.create(name: 'H3')
Unit.create(name: 'GTU')
Unit.create(name: '4N')
Actionitem.create(category: 'cat1', description: 'Category1', complete: 'false')
Actionitem.create(category: 'cat2', description: 'Category2', complete: 'false')
Actionitem.create(category: 'cat3', description: 'Category3', complete: 'false')
Actionitem.create(category: 'cat4', description: 'Category4', complete: 'false')

