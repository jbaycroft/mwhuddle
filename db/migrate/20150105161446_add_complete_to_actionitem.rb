class AddCompleteToActionitem < ActiveRecord::Migration
  def change
    add_column :actionitems, :complete, :boolean
  end
end
