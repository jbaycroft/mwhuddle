class CreateActionitems < ActiveRecord::Migration
  def change
    create_table :actionitems do |t|
      t.string :category
      t.string :description
      t.string :assignedto
      t.datetime :duedate
      t.timestamps
    end
  end
end
