class CreateOrs < ActiveRecord::Migration
  def change
    create_table :ors do |t|
      t.integer :cases, default: 0
      t.timestamps
    end
  end
end
