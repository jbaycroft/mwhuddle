class AddDefaultValueToCompleteAttribute < ActiveRecord::Migration
  def change
  	change_column :actionitems, :complete, :boolean, :default => false
  end
end
