class CreateEds < ActiveRecord::Migration
  def change
    create_table :eds do |t|
      t.integer :level, default: 0
      t.integer :census, default: 0
      t.timestamps
    end
  end
end
