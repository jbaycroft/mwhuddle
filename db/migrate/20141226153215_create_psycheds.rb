class CreatePsycheds < ActiveRecord::Migration
  def change
    create_table :psycheds do |t|
      t.integer :lmh, default: 0
      t.integer :fuh, default: 0
      t.integer :stv, default: 0
      t.timestamps
    end
  end
end
