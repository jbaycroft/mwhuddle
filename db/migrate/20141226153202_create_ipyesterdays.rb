class CreateIpyesterdays < ActiveRecord::Migration
  def change
    create_table :ipyesterdays do |t|
      t.integer :plandcprenoon, default: 0
      t.integer :actualdcprenoon, default: 0
      t.integer :plandcafternoon, default: 0
      t.integer :actualdcafternoon, default: 0
      t.integer :admithib, default: 0
      t.integer :longestwaited, default: 0
      t.integer :longestwaitpacu, default: 0
      t.references :unit
      t.timestamps
    end
  end
end
