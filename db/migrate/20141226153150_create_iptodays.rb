class CreateIptodays < ActiveRecord::Migration
  def change
    create_table :iptodays do |t|
      t.integer :staffedcap, default: 0
      t.integer :actualcensus, default: 0
      t.integer :boarders, default: 0
      t.integer :surgadmit, default: 0
      t.integer :plannedadmit, default: 0
      t.integer :plandcprenoon, default: 0
      t.integer :plandcafternoon, default: 0
      t.integer :adjustedcensus, default: 0
      t.integer :precautionpat, default: 0
      t.integer :singlegroup, default: 0
      t.integer :psych, default: 0
      t.references :unit
      t.timestamps
    end
  end
end
