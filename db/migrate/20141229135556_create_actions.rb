class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.string :category
      t.string :description
      t.string :assigned
      t.datetime :duedate
      t.timestamps
    end
  end
end
