# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150106162643) do

  create_table "actionitems", force: true do |t|
    t.string   "category"
    t.string   "description"
    t.string   "assignedto"
    t.datetime "duedate"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "complete",    default: false
  end

  create_table "actions", force: true do |t|
    t.string   "category"
    t.string   "description"
    t.string   "assigned"
    t.datetime "duedate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "eds", force: true do |t|
    t.integer  "level",      default: 0
    t.integer  "census",     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "iptodays", force: true do |t|
    t.integer  "staffedcap",      default: 0
    t.integer  "actualcensus",    default: 0
    t.integer  "boarders",        default: 0
    t.integer  "surgadmit",       default: 0
    t.integer  "plannedadmit",    default: 0
    t.integer  "plandcprenoon",   default: 0
    t.integer  "plandcafternoon", default: 0
    t.integer  "adjustedcensus",  default: 0
    t.integer  "precautionpat",   default: 0
    t.integer  "singlegroup",     default: 0
    t.integer  "psych",           default: 0
    t.integer  "unit_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ipyesterdays", force: true do |t|
    t.integer  "plandcprenoon",     default: 0
    t.integer  "actualdcprenoon",   default: 0
    t.integer  "plandcafternoon",   default: 0
    t.integer  "actualdcafternoon", default: 0
    t.integer  "admithib",          default: 0
    t.integer  "longestwaited",     default: 0
    t.integer  "longestwaitpacu",   default: 0
    t.integer  "unit_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ors", force: true do |t|
    t.integer  "cases",      default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "psycheds", force: true do |t|
    t.integer  "lmh",        default: 0
    t.integer  "fuh",        default: 0
    t.integer  "stv",        default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "units", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
